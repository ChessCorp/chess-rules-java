package org.alcibiade.chess.integration;

import org.alcibiade.chess.model.*;
import org.alcibiade.chess.persistence.PgnMarshaller;
import org.alcibiade.chess.rules.ChessHelper;
import org.alcibiade.chess.rules.ChessRules;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath:/org/alcibiade/chess/integration/testContext.xml"})
public class ShortestGameTest {
    
    @Autowired
    private ChessRules chessRules;
    
    @Autowired
    private PgnMarshaller pgnMarshaller;
    
    @Test
    public void testGame() throws PgnMoveException, IllegalMoveException {
        String[] history = {"f3", "e5", "g4", "Qh4"};
        ChessPosition position = chessRules.getInitialPosition();
        
        for (String pgnMove : history) {
            ChessMovePath path = pgnMarshaller.convertPgnToMove(position, pgnMove);
            position = ChessHelper.applyMoveAndSwitch(chessRules, position, path);
        }
        
        Assertions.assertThat(chessRules.getStatus(position)).isEqualTo(ChessGameStatus.BLACKWON);
        Assertions.assertThat(chessRules.getAvailableMoves(position)).isEmpty();
    }
}
