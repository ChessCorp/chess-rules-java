package org.alcibiade.chess.integration;

import org.alcibiade.chess.engine.process.ExternalProcessFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Test external process integration in the Spring context.
 */

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath:/org/alcibiade/chess/integration/testContext.xml"})
public class ProcessIntegrationTest {

    @Autowired
    private ExternalProcessFactory externalProcessFactory;

    @Test
    public void testTimeout() {
        Assertions.assertThat(externalProcessFactory.getTimeout()).isEqualTo(30_000);
    }
}
