package org.alcibiade.chess.model;

public class ChessPositionRenderer {

    public static String renderPositionAsString(ChessPosition position) {
        StringBuilder text = new StringBuilder();
        text.append(position.getNextPlayerTurn());
        text.append(' ');
        text.append(position.isCastlingAvailable(ChessSide.WHITE, true) ? "K" : "");
        text.append(position.isCastlingAvailable(ChessSide.WHITE, false) ? "Q" : "");
        text.append(position.isCastlingAvailable(ChessSide.BLACK, true) ? "k" : "");
        text.append(position.isCastlingAvailable(ChessSide.BLACK, false) ? "q" : "");

        ChessBoardCoord lastPawnDMove = position.getLastPawnDMove();
        if (lastPawnDMove != null) {
            text.append(' ');
            text.append(lastPawnDMove.getPgnCoordinates());
        }

        text.append('\n');

        for (int row = 7; row >= 0; row--) {
            for (int col = 0; col < 8; col++) {
                ChessPiece piece = position.getPiece(new ChessBoardCoord(col, row));
                if (piece == null) {
                    text.append('.');
                } else {
                    text.append(piece.getAsSingleCharacter());
                }

                text.append(' ');
            }

            text.append('\n');
        }

        return text.toString();
    }
}
